npm run build
tar -cvf build.tar.gz dist
scp -i ~/.ssh/id_rsa build.tar.gz ubuntu@grit.dunice-testing.com:/home/ubuntu/test
ssh -i ~/.ssh/id_rsa ubuntu@grit.dunice-testing.com 'cd test && rm -rf build && tar -xvf build.tar.gz && mv dist/my-dream-test-app build'
